import React from 'react';
import { Provider } from 'react-redux';

import { store } from './redux/createStore';
import AppRoutes from './routers';

import './App.css';

const App = props => (
  <Provider store={store}>
    <AppRoutes dispatch={store.dispatch} {...props} />
  </Provider>
);

export default App;
