import React from 'react';
import { connect } from 'react-redux';

import {
  originlinkSelector,
  shortenlinkSelector,
  errorMessageSelector,
  clicksSelector,
  updateOriginLinkAction,
  createShortenLinkAction,
  updateErrorMessageAction,
} from './state';

import { buildPath } from '../network/request';
import './index.css';

const Home = ({
  originLink,
  shortenLink,
  clicks,
  shortenLinkAC,
  errorMessage,
  updateOriginLink,
  updateErrorMessage,
}) => (
  <div className='Home-wrapper'>
    <h3 style={{ textAlign: 'center' }}>Shorten your link by a click!</h3>
    <div className='Home-actions'>
      <input
        type='text'
        value={originLink}
        onChange={e => (updateOriginLink(e.target.value), updateErrorMessage(''))} />
      <div className='Home-button' onClick={shortenLinkAC}>
        Shorten
      </div>
    </div>
    {errorMessage.length ? <div className='Home-error'>{errorMessage}</div> : null}
    { shortenLink ? <div className='Home-result'>
      Shorten link: <a href={buildPath(shortenLink)}>{buildPath(shortenLink)}</a>
      <div>Clicks: <a>{clicks}</a></div>
    </div> : null}
  </div>
);

const enhance = connect(
  state => ({
    originLink: originlinkSelector(state),
    shortenLink: shortenlinkSelector(state),
    errorMessage: errorMessageSelector(state),
    clicks: clicksSelector(state),
  }),
  {
    updateOriginLink: updateOriginLinkAction,
    updateErrorMessage: updateErrorMessageAction,
    shortenLinkAC: createShortenLinkAction,
  }
);

export default enhance(Home);
