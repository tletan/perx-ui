import { combineReducers } from 'redux';
import { Observable } from 'rxjs';
import { handleActions } from 'redux-actions';
import { ajax } from 'rxjs/observable/dom/ajax';
import { get } from 'lodash/fp';
import { createAction } from 'redux-actions';
import { reduce } from 'lodash';

import { buildApiUrl } from '../network/request';

const UPDATE_ORIGIN_LINK = 'home/updateOriginLink';
const UPDATE_LINK = 'home/updateLink';
const UPDATE_ERROR_MESSAGE = 'home/updateErrorMessage';

export const originlinkSelector = get('home.originLink');
export const shortenlinkSelector = get('home.shortenLink');
export const clicksSelector = get('home.clicks');
export const errorMessageSelector = get('home.errorMessage');

const CREATE_SHORTEN_LINK = 'home/createShortenLink';
export const createShortenLinkAction = createAction(CREATE_SHORTEN_LINK);

export const updateOriginLinkAction = createAction(UPDATE_ORIGIN_LINK);
export const updateLinkAction = createAction(UPDATE_LINK);
export const updateErrorMessageAction = createAction(UPDATE_ERROR_MESSAGE);

const originLink = handleActions({
  [UPDATE_ORIGIN_LINK]: (state, { payload }) => payload,
}, '');

const shortenLink = handleActions({
  [UPDATE_LINK]: (state, { payload }) => payload.shorten_link,
}, '');

const clicks = handleActions({
  [UPDATE_LINK]: (state, { payload }) => payload.clicks,
}, '');

const errorMessage = handleActions({
  [UPDATE_ERROR_MESSAGE]: (state, { payload }) => payload,
}, '');

const reducer = combineReducers({
  originLink,
  shortenLink,
  clicks,
  errorMessage,
});

const display = errors => reduce(errors.response, (result, value, key) => `${key}: ${value}, ${result}`, '')

export const createShortenLinkEpic = (action$, store) =>
  action$.ofType(CREATE_SHORTEN_LINK)
    .mergeMap(_ =>
      ajax.post(buildApiUrl('links'), { link: originlinkSelector(store.getState()) })
      .pluck('response')
      .map(json => updateLinkAction(json))
      .catch(error => Observable.of({
          type: UPDATE_ERROR_MESSAGE,
          payload: display(error),
          error: true,
        }))
    )

export default { home: reducer };
