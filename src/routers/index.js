import React from 'react';
import { Route, Router } from 'react-router';

import history from './history';
import Home from '../Home';

const AppRouters = ({ dispatch }) => (
  <Router history={history}>
    <div>
      <Route
        exact
        path="/"
        render={ () => {
            return <Home />
          }
        }
      />
   </div>
  </Router>
);

export default AppRouters;
