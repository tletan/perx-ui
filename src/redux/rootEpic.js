import { combineEpics } from 'redux-observable';
import { createShortenLinkEpic } from '../Home/state';

let epicRegistry = [
  createShortenLinkEpic,
];

export default combineEpics(...epicRegistry);
