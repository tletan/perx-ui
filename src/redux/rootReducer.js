import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import home from '../Home/state';

export const rootReducer = combineReducers({
  routing: routerReducer,
  ...home,
});

export default rootReducer;
