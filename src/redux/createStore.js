import { createStore as createReduxStore, applyMiddleware, compose } from 'redux';
import { compact } from 'lodash';

import rootReducer from './rootReducer';
import arrayMiddleware from './middlewares/array-middleware';
import routerMiddleware from './middlewares/router-middleware';
import epicMiddleware from './middlewares/epic-middleware';

const enhancers = compact([
  applyMiddleware(
    arrayMiddleware,
    routerMiddleware,
    epicMiddleware,
  ),
  window.devToolsExtension && window.devToolsExtension(),
]);

export const createStore = () => {
  const store = createReduxStore(
    rootReducer,
    compose(...enhancers),
  );

  return store;
};

export const store = createStore();
