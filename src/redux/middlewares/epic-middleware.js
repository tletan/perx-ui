import { createEpicMiddleware } from 'redux-observable';

import rootEpic from '../rootEpic';
import history from '../../routers/history';

const middleware = createEpicMiddleware(rootEpic, {
  dependencies: { history },
});

export default middleware;
