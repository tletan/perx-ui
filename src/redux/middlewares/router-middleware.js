import { routerMiddleware } from 'react-router-redux';
import history from '../../routers/history';

const middleware = routerMiddleware(history)

export default middleware;

