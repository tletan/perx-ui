
export const serverHost = "http://localhost:4000";
//export const server_host = "";
//
export const buildApiUrl = path => `${serverHost}/api/v1/${path}`;
export const buildPath = path => (path ? `${serverHost}/${path}` : '');
